import React from 'react';
import './App.css';
import Header from './components/header';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './components/home';
import Boards from './components/boards';
import Lists from './components/lists';

// require('dotenv').config();

function App() {
  // console.log(process.env.key);
  return (
    <Router>
      <React.Fragment>
        <Header />
        <Route path="/" exact component={Home} />
        <Route path="/boards" exact component={Boards} />
        <Route path="/boards/:id" exact component={Lists} />
      </React.Fragment>
    </Router>
  );
}

export default App;
