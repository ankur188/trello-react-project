import React, { Component } from 'react';
import Board from './board';

const token =
  '7da319ed908fadf5b55ee42b11f47c77a3dc1a29a2b5adc324bc772ae100a08c';
const key = '32851db67dc0552bd9418f377e9d7ce6';

class Boards extends Component {
  state = {
    boards: []
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/ankurtiwari20/boards?key=${key}&token=${token}`,
      {
        method: 'GET'
      }
    )
      .then(response => response.json())
      .then(data => {
        this.setState({
          boards: data
        });
      })
      .catch(err => console.log(err));
  }

  render() {
    let boards = this.state.boards.map(board => {
      return <Board key={board.id} board={board} />;
    });

    return boards;
  }
}

export default Boards;
