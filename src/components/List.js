import React, { Component } from 'react';
import Card from './card';
import CardForm from './card-form';

const token =
  '7da319ed908fadf5b55ee42b11f47c77a3dc1a29a2b5adc324bc772ae100a08c';
const key = '32851db67dc0552bd9418f377e9d7ce6';

class List extends Component {
  state = {
    cards: [],
    addForm: false,
    addCardInput: ''
  };

  onAddCard = () => {
    this.setState({
      addForm: true
    });
  };

  closeAdd = () => {
    this.setState({
      addForm: false
    });
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/lists/${this.props.list.id}/cards?key=${key}&token=${token}`,
      {
        method: 'GET'
      }
    )
      .then(Response => Response.json())
      .then(data => {
        this.setState({
          cards: data
        });
      });
  }

  getInputValueToAdd = e => {
    this.setState({
      addCardInput: e
    });
  };

  addRequest = async () => {
    let allCards = this.state.cards;
    await fetch(
      `https://api.trello.com/1/cards?name=${this.state.addCardInput}&idList=${this.props.list.id}&keepFromSource=all&key=${key}&token=${token}`,
      {
        method: 'POST'
      }
    )
      .then(Response => Response.json())
      .then(data => {
        allCards.push(data);
      })
      .then(() =>
        this.setState({
          cards: allCards,
          addCardInput: ''
        })
      );
  };

  deleteCard = (event, id) => {
    event.stopPropagation();
    let allCards = this.state.cards;
    fetch(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`, {
      method: 'DELETE'
    }).then(() => {
      let afterDeleteAllCards = allCards.filter(card => card.id !== id);
      this.setState({
        cards: afterDeleteAllCards
      });
    });
  };

  render() {
    let cards = this.state.cards.map(card => {
      return (
        <Card
          key={card.id}
          card={card}
          deleteCard={this.deleteCard}
          openModal={this.props.openModal}
        />
      );
    });

    let openAddForm = this.state.addForm ? 'block' : 'none';
    let addFormButton = this.state.addForm ? 'none' : 'block';

    return (
      <div className="List-box" listid={this.props.list.id}>
        <div className="list-header">
          <h5 className=" list-title">{this.props.list.name}</h5>
          <button
            onClick={() => this.props.deleteList(this.props.list.id)}
            className="btn btn-xsm mr-3"
          >
            X
          </button>
        </div>
        <div className="list">{cards}</div>
        <button
          onClick={this.onAddCard}
          className="btn btn-default add-card"
          style={{ display: addFormButton }}
        >
          + Add a card
        </button>
        <CardForm
          buttonTitle="card"
          style={{ display: openAddForm }}
          closeAdd={this.closeAdd}
          getInputValueToAdd={this.getInputValueToAdd}
          addRequest={this.addRequest}
          inputvalue={this.state.addCardInput}
        />
      </div>
    );
  }
}

export default List;
