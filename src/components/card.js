import React, { Component } from 'react';

class Card extends Component {
  state = {};
  render() {
    return (
      <div
        onClick={() => this.props.openModal(this.props.card)}
        className="card"
      >
        <h3 className="card-title">{this.props.card.name}</h3>
        <button
          onClick={event => this.props.deleteCard(event, this.props.card.id)}
          className="btn btn-default btn-xsm delete-card"
        >
          <i className="fa fa-trash"></i>
        </button>
      </div>
    );
  }
}

export default Card;
