import React, { Component } from 'react';

class CardForm extends Component {
  state = {};

  render() {
    return (
      <div
        className="add-card-form"
        style={{ display: this.props.style.display }}
      >
        <div className="add-card-menu card">
          <input
            type="text"
            placeholder="Enter title"
            className="add-card-menu-input"
            onChange={e => this.props.getInputValueToAdd(e.target.value)}
            style={{ width: '100%' }}
            value={this.props.inputvalue}
          />
        </div>
        <div className="add-card-buttons">
          <button
            onClick={this.props.addRequest}
            className="btn btn-success button-add"
          >
            {`Add ${this.props.buttonTitle}`}
          </button>
          <button
            onClick={this.props.closeAdd}
            className="btn btn-danger button-close"
          >
            X
          </button>
        </div>
      </div>
    );
  }
}

export default CardForm;
