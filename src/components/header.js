import React from 'react';
import { Link } from 'react-router-dom';

function Header() {
  return (
    <header>
      <Link to="/">
        <img
          src="https://img.icons8.com/windows/32/000000/home-page.png"
          className="btn-secondary home-logo"
          alt=""
        ></img>
      </Link>
      <Link to="/boards">
        <button className="btn btn-secondary boards-button">Boards</button>
      </Link>
      <img
        className="trello-logo"
        src="https://a.trellocdn.com/prgb/dist/images/header-logo-2x.01ef898811a879595cea.png"
        alt=""
      />
    </header>
  );
}
export default Header;
