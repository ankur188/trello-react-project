import React from 'react';

function Home() {
  let styles = {
    color: 'gray',
    display: 'flex',
    justifyContent: 'center',
    marginTop: '1vw',
    fontSize: '6vw'
  };
  return <h1 style={styles}>Welcome to Trello</h1>;
}
export default Home;
