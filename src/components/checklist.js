import React, { Component } from 'react';
import Items from './checkItems';

const token =
  '7da319ed908fadf5b55ee42b11f47c77a3dc1a29a2b5adc324bc772ae100a08c';
const key = '32851db67dc0552bd9418f377e9d7ce6';

class CheckList extends Component {
  state = {
    checkItemsForm: false,
    checkItems: [],
    inputTitle: ''
  };

  addItemFormDispalyButton = () => {
    this.setState({
      checkItemsForm: true
    });
  };

  addItemFormCloseDisplayButton = () => {
    this.setState({
      checkItemsForm: false
    });
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checklist.id}/checkItems?key=${key}&token=${token}`,
      {
        method: 'GET'
      }
    )
      .then(Response => Response.json())
      .then(data => {
        this.setState({
          checkItems: data
        });
      });
  }

  inputTitle = e => {
    this.setState({
      inputTitle: e.target.value
    });
  };

  addItemRequest = () => {
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checklist.id}/checkItems?name=${this.state.inputTitle}&pos=bottom&checked=false&key=${key}&token=${token}`,
      {
        method: 'POST'
      }
    )
      .then(Response => Response.json())
      .then(data => {
        this.setState({
          checkItemsForm: false,
          checkItems: this.state.checkItems.concat([data]),
          inputTitle: ''
        });
      });
  };

  deleteItem = id => {
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checklist.id}/checkItems/${id}?key=${key}&token=${token}`,
      {
        method: 'DELETE'
      }
    ).then(() => {
      let remainingItems = this.state.checkItems.filter(item => item.id !== id);
      this.setState({
        checkItems: remainingItems
      });
    });
  };

  updateItemCheckBox = (event, item) => {
    let checkInput = event.target.checked ? 'complete' : 'incomplete';
    fetch(
      `https://api.trello.com/1/cards/${this.props.checklist.idCard}/checkItem/${item.id}?state=${checkInput}&key=${key}&token=${token}`,
      {
        method: 'PUT'
      }
    )
      .then(Response => Response.json())
      .then(data => {
        let allItems = this.state.checkItems;
        allItems[allItems.indexOf(item)].state = data.state;
        this.setState({
          checkItems: allItems
        });
      });
  };

  render() {
    let addItemFormButton = this.state.checkItemsForm ? 'none' : 'block';
    let addItemForm = this.state.checkItemsForm ? 'block' : 'none';

    let allCheckItems = this.state.checkItems.map(item => {
      return (
        <Items
          key={item.id}
          item={item}
          deleteItem={this.deleteItem}
          updateItemCheckBox={this.updateItemCheckBox}
        />
      );
    });

    return (
      <div className="check-list-added">
        <div id={this.props.checklist.id}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <h5>{this.props.checklist.name}</h5>
            <button
              onClick={() =>
                this.props.deleteCheckList(this.props.checklist.id)
              }
              className="btn btn-xsm delete-checklist"
            >
              <i className="fa fa-trash"></i>
            </button>
          </div>
          <div className="check-list-items">{allCheckItems}</div>
          <div className="check-list-buttons">
            <button
              onClick={this.addItemFormDispalyButton}
              className="btn btn-primary btn-sm add-checklist-item"
              style={{ display: addItemFormButton }}
            >
              Add an item
            </button>
          </div>
          <div
            className="add-check-list-item-form"
            style={{ display: addItemForm }}
          >
            <input
              onChange={this.inputTitle}
              type="text"
              placeholder="Enter title for this checklist item"
              className="add-check-list-item-input"
              value={this.state.inputTitle}
            />
            <div>
              <button
                onClick={this.addItemRequest}
                className="add-item-to-checklist btn btn-primary btn-xsm"
              >
                Add item
              </button>
              <button
                onClick={this.addItemFormCloseDisplayButton}
                className="btn btn-xsm cancel-add-item-form"
              >
                X
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckList;
