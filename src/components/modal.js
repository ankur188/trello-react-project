import React from 'react';
import Modal from 'react-responsive-modal';
import Checklist from './checklist';
import Form from './card-form';

const token =
  '7da319ed908fadf5b55ee42b11f47c77a3dc1a29a2b5adc324bc772ae100a08c';
const key = '32851db67dc0552bd9418f377e9d7ce6';

const styles = {
  fontFamily: 'sans-serif',
  textAlign: 'center'
};

class ModalComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkList: [],
      checkListForm: false,
      addListInput: ''
    };
  }

  componentDidUpdate() {
    if (this.props.card.id !== this.state.prevCardId) {
      fetch(
        `https://api.trello.com/1/cards/${this.props.card.id}/checklists?checkItems=none&key=${key}&token=${token}`,
        {
          method: 'GET'
        }
      )
        .then(Response => Response.json())
        .then(data => {
          console.log(data);
          this.setState({
            checkList: data,
            prevCardId: this.props.card.id
          });
        });
    }
  }

  getInputValueToAdd = e => {
    this.setState({
      addListInput: e
    });
  };

  addRequest = async () => {
    if (this.state.addListInput !== '') {
      await fetch(
        `https://api.trello.com/1/cards/${this.props.card.id}/checklists?name=${this.state.addListInput}&key=${key}&token=${token}`,
        {
          method: 'POST'
        }
      )
        .then(Response => Response.json())
        .then(data => {
          console.log(data);

          this.setState({
            checkList: this.state.checkList.concat([data]),
            addListInput: '',
            checkListForm: false
          });
        });
    }
  };

  openAddCheckListForm = () => {
    this.setState({
      checkListForm: true
    });
  };

  closeAddCheckListForm = () => {
    this.setState({
      checkListForm: false
    });
  };

  deleteCheckList = async id => {
    await fetch(
      `https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`,
      {
        method: 'DELETE'
      }
    ).then(() => {
      let remainingChecklist = this.state.checkList.filter(
        checkList => checkList.id !== id
      );
      this.setState({
        checkList: remainingChecklist
      });
    });
  };

  render() {
    let addCheckListFormDisplay = this.state.checkListForm ? 'none' : 'block';
    let addCheckListFormCloseDisplay = this.state.checkListForm
      ? 'block'
      : 'none';

    let allChecklistForACard = this.state.checkList.map(checkList => {
      return (
        <Checklist
          key={checkList.id}
          checklist={checkList}
          deleteCheckList={this.deleteCheckList}
        />
      );
    });
    return (
      <div style={styles}>
        <Modal open={this.props.openModal} onClose={this.props.closeModal}>
          <h2>{`${this.props.card.name}`}</h2>

          {allChecklistForACard}
          <button
            onClick={this.openAddCheckListForm}
            className="btn  add-a-checklist mt-2"
            style={{ display: addCheckListFormDisplay }}
          >
            Add a CheckList
          </button>
          <Form
            closeAdd={this.closeAddCheckListForm}
            buttonTitle="Checklist"
            style={{ display: addCheckListFormCloseDisplay }}
            addRequest={this.addRequest}
            getInputValueToAdd={this.getInputValueToAdd}
            inputvalue={this.state.addListInput}
          />
        </Modal>
      </div>
    );
  }
}

export default ModalComponent;
