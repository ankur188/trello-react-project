import React, { Component } from 'react';
import List from './List';
import Form from './card-form';
import Modal from './modal';
const token =
  '7da319ed908fadf5b55ee42b11f47c77a3dc1a29a2b5adc324bc772ae100a08c';
const key = '32851db67dc0552bd9418f377e9d7ce6';

class Lists extends Component {
  state = {
    lists: [],
    addList: false,
    addListInput: '',
    open: false,
    card: {}
  };

  componentDidMount(props) {
    fetch(
      `https://api.trello.com/1/boards/${this.props.match.params.id}/lists?key=${key}&token=${token}`,
      {
        method: 'GET'
      }
    )
      .then(Response => Response.json())
      .then(data => {
        console.log(data);
        this.setState({
          lists: data
        });
      });
  }

  addListForm = () => {
    this.setState({
      addList: true
    });
  };

  getInputValueToAdd = e => {
    this.setState({
      addListInput: e
    });
  };

  addRequest = async () => {
    let allLists = this.state.lists;
    console.log(this.props.match.params.id);
    await fetch(
      `https://api.trello.com/1/lists?name=${this.state.addListInput}&idBoard=${this.props.match.params.id}&pos=bottom&key=${key}&token=${token}`,
      {
        method: 'POST'
      }
    )
      .then(Response => Response.json())
      .then(data => {
        allLists.push(data);
      })
      .then(() =>
        this.setState({
          lists: allLists,
          addList: false,
          addListInput: ''
        })
      );
  };

  closeAdd = () => {
    this.setState({
      addList: false
    });
  };

  deleteList = id => {
    fetch(
      `https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`,
      {
        method: 'PUT'
      }
    ).then(() =>
      this.setState({
        lists: this.state.lists.filter(list => list.id !== id)
      })
    );
  };

  openModal = cardObj => {
    this.setState({
      open: true,
      card: cardObj
    });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    var formOpenButton = this.state.addList ? 'none' : 'block';
    var formOpen = this.state.addList ? 'block' : 'none';

    let lists = this.state.lists.map(list => {
      return (
        <List
          key={list.id}
          list={list}
          deleteList={this.deleteList}
          openModal={this.openModal}
        />
      );
    });
    return (
      <div className="Lists">
        {lists}
        <button
          onClick={this.addListForm}
          className="btn btn-default add-card add-list"
          style={{ display: formOpenButton }}
        >
          Add another list
        </button>
        <Form
          buttonTitle="list"
          style={{ display: formOpen }}
          getInputValueToAdd={this.getInputValueToAdd}
          addRequest={this.addRequest}
          inputvalue={this.state.addListInput}
          closeAdd={this.closeAdd}
        />
        <Modal
          openModal={this.state.open}
          closeModal={this.onCloseModal}
          card={this.state.card}
        />
      </div>
    );
  }
}

export default Lists;
