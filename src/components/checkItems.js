import React, { Component } from 'react';

class Items extends Component {
  state = {};
  render() {
    return (
      <div className="items">
        <input
          onChange={() => this.props.updateItemCheckBox(event, this.props.item)}
          type="checkbox"
          className="check-box"
          checked={this.props.item.state === 'complete' ? true : false}
          //   readOnly
        />
        <p>{this.props.item.name}</p>
        <button
          onClick={() => this.props.deleteItem(this.props.item.id)}
          className="btn btn-default btn-xsm pull-right delete-item"
        >
          X
        </button>
      </div>
    );
  }
}

export default Items;
