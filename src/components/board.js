import React from 'react';
import { Link } from 'react-router-dom';

const Board = props => {
  return (
    <div>
      <Link to={`/boards/${props.board.id}`} className="container">
        <button
          className="btn btn-lg mt-4 board-names font-weight-bold"
          id={props.board.id}
        >
          {props.board.name}
        </button>
      </Link>
    </div>
  );
};

export default Board;
